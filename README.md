# AI agent for 2 Players Isolation Game

## Overview

This assignment will cover some of the concepts discussed in the Adversarial Search lectures. we will be implementing game playing agents for a variant of the game Isolation called Team Isolation.

### The Game

 The rules of Team Isolation are simple. In our version, two players take turn placing their own game pieces on different squares of a 7-by-7 grid. Each player has 2 game pieces which they can move on the board. At the beginning of the game, the players, in turn, place each of their two pieces on any unoccupied square. The players must use their first two turns to place their two pieces. From that point on, the pieces move like a Queen in chess (any number of squares vertically, horizontally, or diagonally but not past an occupied or blocked square). Each time a player moves their piece, there is a twist. There is a chance, based on non-uniform probability that increases with distance moved, the piece will fail to reach the intended square. Essentially, the farther away we move, the greater chance we will land on an adjacent valid square to the intended target. All 1 square moves are certain but all other moves have a fixed maximum chance of failure based on distance. The square that was previously occupying is blocked and cannot be moved for remainder of the game.  The queens can’t move through each other or through the blocked portion of the board. The first player who is unable to move loses.

### The Files

While we'll only have to edit and submit `player_submission.py`, there are a number of notable files:

1. `isolation.py`: Includes the `Board` class and a function for printing out a game as text.
2. `player_submission.py`: Where we'll implement required methods for wer agents.
3. `player_submission_tests.py`: Sample tests to validate wer agents locally.
4. `test_players.py`: Example agents used to play isolation locally.


### The Project

wer task is to create an AI that can play and win a game of Team Isolation. wer AI will be tested against several pre-baked AIs as well as wer peers’ AI systems. we will implement wer AI in Python 2.7.

wer goal is to implement the following parts of the AI in the class CustomPlayer:

1. Evaluation functions (`OpenMoveEvalFn()` and `CustomEvalFn()`)
2. The minimax algorithm (`minimax()`)
3. Alpha-beta pruning (`alphabeta()`)

wer agent will have a limited amount of time to act each turn (5 seconds).




### Built-in Tests

In `player_submission_tests.py` several built-in tests can be found in the **`main()`** function. We've included these to help we test wer player and evaluation function as well as to give we an idea of how the classes are used. Feel free to play around with the code and add more tests.

## Helper Player classes (`test_players.py`)

We include 2 player types for we to test against locally:

- `RandomPlayer` - chooses a legal move randomly from among the available legal moves
- `HumanPlayer` - allows *we* to play against the AI
