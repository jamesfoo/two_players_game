#!/usr/bin/env python
from isolation import Board, game_as_text


# This file is your main submission that will be graded against. Do not
# add any classes or functions to this file that are not part of the classes
# that we want.

# Submission Class 1
class OpenMoveEvalFn:

    def score(self, game, maximizing_player_turn=True):
        """Score the current game state
        
        Evaluation function that outputs a score equal to how many 
        moves are open for AI player on the board minus the moves open 
        for opponent player.
            
        Args
            param1 (Board): The board and game state.
            param2 (bool): True if maximizing player is active.

        Returns:
            float: The current state's score. Your agent's moves minus the opponent's moves.
            
        """
        # TODO: finish this function!
        # raise NotImplementedError

        legal_moves = game.get_legal_moves_of_queen1().keys()
        legal_moves += game.get_legal_moves_of_queen2().keys()
        legal_moves_count = len(set(legal_moves))

        opponent_moves = game.get_opponent_moves()
        opponent_moves_queen1 = opponent_moves[game.__inactive_players_queen1__].keys()
        opponent_moves_queen2 = opponent_moves[game.__inactive_players_queen2__].keys()
        opponent_moves_count = len(set(opponent_moves_queen1 + opponent_moves_queen2))

        return legal_moves_count-opponent_moves_count



# Submission Class 2
class CustomEvalFn:

    def __init__(self):
        pass

    def score(self, game, maximizing_player_turn=True):
        """Score the current game state
        
        Custom evaluation function that acts however you think it should. This 
        is not required but highly encouraged if you want to build the best 
        AI possible.
        
        Args
            game (Board): The board and game state.
            maximizing_player_turn (bool): True if maximizing player is active.

        Returns:
            float: The current state's score, based on your own heuristic.
            
        """
        # TODO: finish this function!
        # raise NotImplementedError

        legal_moves = game.get_legal_moves_of_queen1().keys()
        legal_moves += game.get_legal_moves_of_queen2().keys()
        legal_moves_count = len(set(legal_moves))

        opponent_moves = game.get_opponent_moves()
        opponent_moves_queen1 = opponent_moves[game.__inactive_players_queen1__].keys()
        opponent_moves_queen2 = opponent_moves[game.__inactive_players_queen2__].keys()
        opponent_moves_count = len(set(opponent_moves_queen1 + opponent_moves_queen2))


        board_size = game.height * game.width
        left_space = board_size - game.move_count
        if left_space < board_size/2:
            return legal_moves_count - 3*opponent_moves_count
        else:
            return legal_moves_count - opponent_moves_count




class CustomPlayer:
    # TODO: finish this class!
    """Player that chooses a move using 
    your evaluation function and 
    a depth-limited minimax algorithm 
    with alpha-beta pruning.
    You must finish and test this player
    to make sure it properly uses minimax
    and alpha-beta to return a good move
    in less than 5 seconds."""

    def __init__(self, search_depth=3, eval_fn=OpenMoveEvalFn()):
    # def __init__(self, search_depth=3, eval_fn=CustomEvalFn()):        
        """Initializes your player.
        
        if you find yourself with a superior eval function, update the default 
        value of `eval_fn` to `CustomEvalFn()`
        
        Args:
            search_depth (int): The depth to which your agent will search
            eval_fn (function): Utility function used by your agent
        """
        self.eval_fn = eval_fn
        self.search_depth = search_depth

    def move(self, game, legal_moves, time_left):
        """Called to determine one move by your agent
        
        Args:
            game (Board): The board and game state.
            legal_moves (dict): Dictionary of legal moves and their outcomes
            time_left (function): Used to determine time left before timeout
            
        Returns:
            (tuple, int): best_move, best_queen
        """
        best_move, best_queen, utility = self.minimax(game, time_left, depth=self.search_depth)
        # best_move, best_queen, utility = self.alphabeta(game, time_left, depth=self.search_depth)        
        # change minimax to alphabeta after completing alphabeta part of assignment
        return best_move, best_queen

    def utility(self, game):
        """Can be updated if desired"""
        return self.eval_fn.score(game)

    def minimax(self, game, time_left, depth=float("inf"), maximizing_player=False):
        """Implementation of the minimax algorithm
        
        Args:
            game (Board): A board and game state.
            time_left (function): Used to determine time left before timeout
            depth: Used to track how deep you are in the search tree
            maximizing_player (bool): True if maximizing player is active.

        Returns:
            (tuple, int, int): best_move, best_queen, best_val
        """
        # TODO: finish this function!
        # raise NotImplementedError


        # if time_left() <= 10:
        #     return (-1,-1), game.get_active_players_queen()[0], None

        from random import randint

        if game.move_count == 0:
            legal_moves = game.get_legal_moves_of_queen1()
            m = legal_moves.keys()[randint(0, len(legal_moves)-1)]
            q = game.get_active_players_queen()[0]
            return m, q, 0

        if game.move_count == 2:
            legal_moves = game.get_legal_moves_of_queen2()
            m = legal_moves.keys()[randint(0, len(legal_moves)-1)]
            q = game.get_active_players_queen()[1]
            return m, q, 0

        from copy import deepcopy

        def combine_legal_moves(game):            
            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            # print legal_moves
            legal_moves_queen2 = game.get_legal_moves_of_queen2()

            legal_moves = deepcopy(legal_moves_queen1)
            legal_moves.update(legal_moves_queen2)

            return legal_moves

        def combine_oppoent_moves(game):
            opponent_moves = game.get_opponent_moves()
            opponent_moves_queen1 = opponent_moves[game.__inactive_players_queen1__]
            opponent_moves_queen2 = opponent_moves[game.__inactive_players_queen2__]

            opp_moves_copy = deepcopy(opponent_moves_queen1)
            opp_moves_copy.update(opponent_moves_queen2)
            return opp_moves_copy

        def find_queen(game, move):
            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            legal_moves_queen2 = game.get_legal_moves_of_queen2()
            if (move in legal_moves_queen1.keys()) and (move in legal_moves_queen2.keys()):
                queen = randint(game.__active_players_queen1__,game.__active_players_queen2__)
            elif move in legal_moves_queen1.keys():
                queen = game.get_active_players_queen()[0]
            else:
                queen = game.get_active_players_queen()[1]
            return queen

        def terminal_test(legal_moves_queen1, legal_moves_queen2, time_left, depth):
            return (len(legal_moves_queen1)+len(legal_moves_queen2))==0 or time_left()<100 or depth == 0

        def min_value(game, time_left, depth):
            # legal_moves = combine_legal_moves(game)

            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            legal_moves_queen2 = game.get_legal_moves_of_queen2()

            if terminal_test(legal_moves_queen1, legal_moves_queen2, time_left, depth):
                #******************************************
                # ! Important: using negative of utility !
                # Reason: In min procedure, active player is the opponent, 
                #         Utility = opponent_move_count - player_move_count
                #         In the view of player, the utility is defined as player_move_count - opponent_move_count
                #******************************************
                return -self.utility(game)

            value1 = float('inf')
            q1 = game.__active_players_queen1__            
            for m in legal_moves_queen1.keys():
                value1 = min(value1, max_value(game.forecast_move(m,q1), time_left, depth-1))
            value2 = float('inf')
            q2 = game.__active_players_queen2__
            for m in legal_moves_queen2.keys():
                value2 = min(value2, max_value(game.forecast_move(m,q2), time_left, depth-1))

            # if value1 < value2:
            #     return q1,value1
            # else: return q2, value2
            return min(value1, value2)

        def max_value(game, time_left, depth):
            # legal_moves = combine_legal_moves(game)            
            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            legal_moves_queen2 = game.get_legal_moves_of_queen2()
            if terminal_test(legal_moves_queen1, legal_moves_queen2, time_left, depth):
                return self.utility(game)

            value1 = -float('inf')
            q1 = game.__active_players_queen1__
            for m in legal_moves_queen1.keys():
                value1 = max(value1, min_value(game.forecast_move(m,q1), time_left, depth-1))
            value2 = -float('inf')
            q2 = game.__active_players_queen2__
            for m in legal_moves_queen2.keys():
                value2 = max(value2, min_value(game.forecast_move(m,q2), time_left, depth-1))
            # if value1 > value2:
            #     return q1, value1
            # else: 
            #     return q2, value2
            return max(value1, value2)

        # result_tuple = ((-1,-1), None, -float('inf'))
        best_val = -float('inf')
        best_queen = None
        best_move = (-1,-1)
        legal_moves_queen1 = game.get_legal_moves_of_queen1()
        legal_moves_queen2 = game.get_legal_moves_of_queen2()


        if (len(legal_moves_queen1)+len(legal_moves_queen2))==0:
            return (-1,-1), None, None

        # print("legal moves:")
        # print(len(legal_moves))
        # print(legal_moves)

        # opponent_moves = combine_oppoent_moves(game)
        # print("opponent moves:")
        # print(len(opponent_moves))
        # print(opponent_moves)

        # print("avaialbel move and their score and queen\n")


        # for m in legal_moves:
        #     q = find_queen(game,m)
        #     new_game = game.forecast_move(m,q)
        #     r = min_value(new_game, time_left, depth-1)
        #     print((q,m,r))
        #     if r  > best_val:
        #         best_val = r
        #         best_queen = q
        #         best_move = m

        # print("final chosen move:\n")
        # print((best_queen, best_move, best_val))

        # print("legal moves for q1: \n")
        # print(legal_moves_queen1)

        # print("legal moves for q2: \n")
        # print(legal_moves_queen2)

        # print("move, score and queen:\n")

        q1 = game.__active_players_queen1__
        best_queen = q1
        for m in legal_moves_queen1:
            v = min_value(game.forecast_move(m,q1), time_left, depth-1)
            if v > best_val:
                best_val = v
                best_move = m
            # print((q1,m,v))

        q2 = game.__active_players_queen2__
        for m in legal_moves_queen2:
            v = min_value(game.forecast_move(m,q2), time_left, depth-1)
            # if v == best_val:
            #     q = randint(q1,q2)
            #     print("equal, chose:",q2)
            #     if q==q2:
            #         best_move = m
            #         best_qeen = q2
            if v >= best_val:
                best_val = v
                best_move = m
                best_queen = q2
        #     print((q2, m,v))
        # print("final chosen:\n")
        # print(best_move, best_queen, best_val)
        return best_move, best_queen, best_val 


    def alphabeta(self, game, time_left, depth=float("inf"), alpha=float("-inf"), beta=float("inf"),
                  maximizing_player=True):
        """Implementation of the alphabeta algorithm
        
        Args:
            game (Board): A board and game state.
            time_left (function): Used to determine time left before timeout
            depth: Used to track how deep you are in the search tree
            alpha (float): Alpha value for pruning
            beta (float): Beta value for pruning
            maximizing_player (bool): True if maximizing player is active.

        Returns:
            (tuple, int, int): best_move, best_queen, best_val
        """
        # TODO: finish this function!

        from copy import deepcopy

        def combine_legal_moves(game):            
            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            # print legal_moves
            legal_moves_queen2 = game.get_legal_moves_of_queen2()

            legal_moves = deepcopy(legal_moves_queen1)
            legal_moves.update(legal_moves_queen2)
            return legal_moves

        def find_queen(game, move):
            legal_moves_queen1 = game.get_legal_moves_of_queen1()
            legal_moves_queen2 = game.get_legal_moves_of_queen2()
            if move in legal_moves_queen1.keys():
                queen = game.get_active_players_queen()[0]
            else:
                queen = game.get_active_players_queen()[1]
            return queen

        def terminal_test(game, legal_moves, time_left, depth):
            return len(legal_moves)==0 or time_left()<50 or depth == 0

        def min_value(game, time_left, depth, alpha, beta):
            legal_moves = combine_legal_moves(game)
            if terminal_test(game, legal_moves, time_left, depth):
                # return negative utility,
                # since the sign of a state's value respective to player and opponent are opposite.
                # mini_value is for opponent, whose unitilty should have opposite sign as max_value's utility
                # alternative algorithm: check berkeley slides, same as max_value, but switch alpha and beta
                return -self.utility(game)
            value = float('inf')
            for m in legal_moves.keys():
                q = find_queen(game, m)
                value = min(value, max_value(game.forecast_move(m,q), time_left, depth-1, alpha, beta))
                if value <= alpha:
                    return value
                else:
                    beta = min(beta, value)
            return value

        def max_value(game, time_left, depth, alpha, beta):
            legal_moves = combine_legal_moves(game)            
            if terminal_test(game, legal_moves, time_left, depth):
                return self.utility(game)

            value = -float('inf')
            for m in legal_moves.keys():
                q = find_queen(game,m)
                value = max(value, min_value(game.forecast_move(m,q), time_left, depth-1, alpha, beta))
                if value >= beta:
                    return value
                else:
                    alpha = max(alpha, value)
            return value

        # result_tuple = ((-1,-1), None, -float('inf'))
        val = -float('inf')
        legal_moves = combine_legal_moves(game)

        if len(legal_moves)==0:
            return (-1,-1), None, None
        
        for m in legal_moves:
            q = find_queen(game,m)
            new_game = game.forecast_move(m,q)
            r = min_value(new_game, time_left, depth-1, alpha, beta)
            if r  > alpha:
                val = r
                best_queen = q
                best_move = m

        # raise NotImplementedError
        return best_move, best_queen, val
